# INFO 4 S8 Project – Dashboard for Overwatch
 
This project was proposed by students wishing to learn new technologies related to the web world.

Our goal is to set up a complete platform, allowing players of the video game Overwatch (created by Blizzard), to enter data and to follow the evolution of their team thanks to the platform. We will also have to set up a database and a back-end, to record, calculate, and retrieve the values necessary for the use of the platform. The site is intended for tournaments supported by the creator of the game, Blizzard, such as AllForOne. The site will have to be translated into several languages (at least French and English). The data can be visible (depending on the choice of the user who created the team) by everyone, via a code, or by person.

# Technologies offered :
**Front-end :**
- Use of the NuxtJS framework to develop a site in JavaScript.

**Back-end :**
- Use of the ExpressJS framework and a MySQL database to store and process data.

Both services run on the same machine. The client must be able to adapt to different types of screens. We will use a technique very much used in JS frameworks which allows to quickly reuse pieces of code already made, the components. The use of dependencies to create graphics, if time allows it will be possible to create some.

# dependency offered :
**Front-end avec NuxtJS :**
- Axios, to make requests to the API (Get method to retrieve, Post method to add a value, Put method to update a value, Delete method to remove a value).
- Momentjs, to process dates.
- Chart.js, to make charts.
- I18n, for translation.
- TailwindCSS, for faster styling.

**Back-end avec ExpressJS :**
- MySQL, allowing to connect to the database and perform queries.
 
# Basic objectives :
- Define the database needed to make statistics.
- Create an authentication system between the front-end and the back-end to manage the access to the platform.
- Create the match addition page with all the necessary fields for data processing.
- Save the data to the database via ExpressJS.
- Process the data to extract the statistics.
- Display the statistics, retrieved by the API (back-end).

# Site hierarchy :
*The user* is defined as a coach. He is identified on the platform by a pseudonym, an email, a date of registration, a description, a rank, as well as a unique number that differentiates him from the others on the platform. A coach can create a maximum of 5 different teams. 

*A team* has a unique number, a name, a description, the id of the referent coach, a creation date, as well as a last activity date. The team can register its social networks (Instagram, Website, Twitch, Twitter, Youtube).

*A team* can register one or more players. Each player has a unique number, his team number, a name, a profile picture, a description, a battleTag and his social networks (Instagram, Website, Twitch, Twitter, Youtube).

*An event* is defined by a unique number and groups, like a folder, matches according to their types. For example: Quick Game, Competitive, UGA Tournaments... A team must create at least one event to add statistics. An event has a name, a description, an image and a display parameter.

*A match* is associated with a game date, an event, a game type, and a map and its type.

*A map* has a starting side and several sides representing the captured points. 

*A composition* is associated with a match, and with a team of 6 players who played this composition. A composition is identified by its unique number and contains the 6 heroes played at the same time.

*A side* has a unique number and an associated match, and contains all the points captured by the team.

*A point* contains the percentage captured, the time to capture the point and the different character changes during the capture of the point.

# Site structure :

- Home page presenting the project
- Registration / login page
- User account management page (add/remove a team, close the user account)
- Team page with global information and statistics:
    - Event page with all the statistics associated with it
        - Page of a match in an event
        - Page of a map in an event
    - Player page with all statistics associated to the player

# Part Calculus and statistics : 

**Desired statistics:**

*Global analysis:*

- Number of games played.
- Overall % of victory of the team.
- Victory % per player of the team.
- Victory % per composition per game played.
- Victory % per hero played by the team. Heroes are classified by type (tank, DPS, Heal)
- Victory % by card type. (2CP, KOTH, Hybrid, Escort)
- % of victory by map.
- Average points (by map type)
    - captured
    - lost

*Event statistics:*

- % of victory by hero played by the team. Heroes are classified by type (tank, DPS, Heal)
- % of victory by type of map

*Finally, note done :*

*Event statistics:*

- Number of games played on the event.
- Overall team win % on the event.
- % of victory of each player on the event.
- % win per composition per game played on the event.
- Victory % per map played during the event.
- Average points (by map type)
    - captured
    - lost

*Player statistics:*

- Number of games played.
- Player's overall win %.
- Victory % per hero played by the player. Heroes are classified by type (tank, DPS, Heal)
- Victory % by card type. (2CP, KOTH, Hybrid, Escort)
- % of victory by map.
- Average points (by map type)
    - captured
    - lost
- Player's % of victory per event in which he participated

# Logbook : 

**17/01/2022 :**

- Drafting of the project description.
- Creation of the database. And implementation of this one in the database of the site using phpMyAdmin.

**24/01/2022 :**
- Creation of the database on phpMyAdmin.
- Distribution of the work:
    - Martin - make the calculations of the statistics
    - Maxime - Do the back-end, get the queries from the front-end and transform them into SQL
    - Etienne - Do the front-end
- Setting up the working environments

**31/01/2022 :**
- Handling of NodeJS and ExpressJS
- Coding of the client part

**07/02/2022 :**
- Creation of the new project structure
- Communication between frontend and backend to add match and create user

**14/02/2022 :**
- Composition registration added
- CompoUsed implementation


**21/02/2022 :**
VACATION TIME 


**28/02/2022 :**
- GetHeroAnalysis implemented 
- Little useful functions added 
- GetCompoTeam added

**07/03/2022 :**
- Finished getHeroAnalysis
- New version of GetMapAnalysis to display all information.
- Need to test Gameplayer

**14/03/2022 :**
- Progress of statistical calculations
- Progress of the site and display

**15/03/2022 :**
- The same as the day before

**21/03/2022 :**
- Finalisation of a first version

**22/03/2022 :**
- Continuation of statistics
